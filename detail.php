<?php session_start(); ?>
<?php require ('koneksi.php'); ?>
<?php 
	// mendapatkan id produk dari url
	$ID_BUKU = $_GET["id"];

	// query ambil data
	$ambil = $koneksi->query("SELECT * FROM BUKU WHERE ID_BUKU = '$ID_BUKU'");
	$detail = $ambil->fetch_assoc();

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>SHOPBOOK</title>
	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>

	<?php include'navbar.php'; ?>

	<section>
		<div class="konten">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img class="img-responsive" src="admin/foto_produk/<?php echo $detail['FOTO']; ?>">
					</div>
					<div class="col-md-6">
						<h2><?php echo $detail["JUDUL"] ?></h2>
						<h4></h4>
						<table class="table table-bordered">
							<tr>
								<th>Harga </th> 
								<td>Rp. <?php echo number_format( $detail["HARGA_JUAL"]); ?></td>
							</tr>
							<tr>
								<th>Penulis </th>
								<td><?php echo $detail["PENULIS"]; ?></td>
							</tr>
							<tr>
								<th>Penerbit </th>
								<td><?php echo $detail["PENERBIT"]; ?></td>
							</tr>
							<tr>
								<th>Tahun </th>
								<td><?php echo $detail["TAHUN"]; ?></td>
							</tr>
							<tr>
								<th>Stock </th>
								<td><?php echo $detail["STOK"]; ?></td>
							</tr>
						</table>
						<form method="post">
							<div class="form-group">
								<div class="input-group">
									<input type="number" min="1" class="form-control" max="<?php echo $detail['STOK'] ?>" name="jumlah" placeholder="masukkan jumlah yang ingin dibeli" required>
									<div class="input-group-btn">
										<button class="btn btn-primary" name="beli">Beli</button>
										<?php
							$ambil=$koneksi->query("SELECT * FROM BUKU WHERE ID_BUKU='$_GET[id]'");
							$us=$ambil->fetch_assoc();
								$STOK=$us['STOK'];

							if ($STOK==0) 
							{
								echo"<script>alert ('Maff stok telah abis');</script>";
								echo "<script>location='index.php';</script>";
							}
							elseif (empty($STOK)) {
								echo "<script>alert ('Masih ada');</script>";
								echo "<script>location='detailproduk.php';</script>";	
							}
							
							
							?>
									</div>
								</div>
							</div>
						</form>
						
						<?php 
						//jika ada tombol beli
						if (isset($_POST["beli"])) 
						{
							//mendapatkan jumlah yg diinputkan
							$jumlah = $_POST["jumlah"]; 
							//masukkan di keranjang belanja
							$_SESSION["keranjang"][$ID_BUKU]= $jumlah;

							echo "<script>alert('produk telah masuk ke keranjang belanja')</script>";
  							echo "<script>location='keranjang.php';</script>";
						}
						 ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>