<?php 
session_start();
  require('koneksi.php');
  
  if (!isset($_SESSION["DISTRIBUTOR"]) OR empty($_SESSION["DISTRIBUTOR"])  ) {
    echo "<script>alert('anda harus login');</script>";
    echo "<script>location='login.php';</script>";
    header('location:login.php');
    exit();
  }
  
  
if (empty($_SESSION['keranjang']))
{
  echo "<script>alert('Anda Belum Belanja, Silahkan Belanja Dulu');</script>";
  echo "<script>location='index.php';</script>";
}
 ?>
 <!DOCTYPE html>
 <html>
 <head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
 </head>
 <body>
   <?php include 'navbar.php'; ?>

 <section class="konten">
  <div class="container">
    <h1>keranjang Belanja</h1>
    <hr>
    <table  width="100%" class="table table-bordered">
      <thead>
        <tr>
          <th>NO</th>
          <th>PRODUK</th>
          <th>HARGA</th>
          <th>JUMLAH</th>
          <th>SUBHARGA</th>
          
        </tr>
      </thead>
      <tbody>
        <?php $nomor=1;?>
        <?php $totalbelanja=0;?>
      <?php foreach($_SESSION['keranjang'] AS $ID_BUKU => $JUMLAH): ?>
      <?php $ambil= $koneksi->query("SELECT * FROM BUKU WHERE ID_BUKU='$ID_BUKU'"); 
      $pecah=$ambil->fetch_assoc();
      $SUBHARGA= $pecah['HARGA_JUAL']*$JUMLAH;
      
      ?> 
        <tr>
          <td><?php echo $nomor; ?></td>
          <td><?php echo $pecah['JUDUL'] ?></td>
          <td>Rp.<?php echo number_format($pecah['HARGA_JUAL']) ?></td>
          <td><?php echo $JUMLAH; ?></td>
          <td>Rp. <?php echo number_format($SUBHARGA); ?></td>
          

        </tr>
        <?php $nomor++; ?>
        <?php $totalbelanja+=$SUBHARGA; ?>
      <?php endforeach ?>
      </tbody>
      <tfoot>
        <tr>
          <th colspan="4" >total Belanja</th>
          <th>Rp. <?php echo number_format($totalbelanja) ?></th>
        </tr>
      </tfoot>        
    </table>
    <form method="post" >
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="" readonly value="<?php echo $_SESSION['DISTRIBUTOR']['NAMA_DISTRIBUTOR']?>" class="form-control" >
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="" readonly value="<?php echo $_SESSION['DISTRIBUTOR']['ALAMAT']?>" class="form-control" >
        </div>
      </div>
      <div class="col-md-4">
        <select class="form-control" name="ID_ONGKIR">
          <option required>PILIH ONGKOS</option>
          <?php

          $ambil=$koneksi->query("SELECT * FROM ONGKIR");
          while($perongkir=$ambil->fetch_assoc()){ ?>
            
            <option value="<?php echo $perongkir['ID_ONGKIR'] ?>">
            <?php echo $perongkir['NAMA_KOTA'] ?>-
            Rp<?php  echo number_format($perongkir['TARIF']) ?>
            </option>
          <?php } ?>

           
        </select>
      </div>
    </div>
    <button class="btn btn-primary" name="checkout">checkout</button>
    </form>
    <?php 
    if (isset($_POST['checkout'])) {
      $ID_DISTRIBUTOR=$_SESSION['DISTRIBUTOR']['ID_DISTRIBUTOR'];
      $ID_ONGKIR = $_POST['ID_ONGKIR'];
      $TANGGAL_PEMBELIAN= date("y-m-d");

      $ambil=$koneksi->query("SELECT * FROM ONGKIR WHERE ID_ONGKIR='$ID_ONGKIR'");
      $ar=$ambil->fetch_assoc();
      $TARIF=$ar['TARIF'];  

      $TOTAL_PEMBELIAN=$totalbelanja+$TARIF;
      
      


      $koneksi->query("INSERT INTO PASOK (ID_PASOK,ID_DISTRIBUTOR,TANGGAL,JUMLAH,ID_ONGKIR) VALUES (NULL,'$ID_DISTRIBUTOR','$TANGGAL_PEMBELIAN','$TOTAL_PEMBELIAN','$ID_ONGKIR')"); 
      
      
      
      $ID_PEMBELIAN_BARUSAN=$koneksi->insert_id;

      foreach($_SESSION['keranjang'] as $ID_BUKU => $JUMLAH) {
        
        $ql="INSERT INTO PEMBELIAN_PRODUK (ID_PASOK,ID_BUKU,JUMLAH_P) VALUES ('$ID_PEMBELIAN_BARUSAN','$ID_BUKU','$JUMLAH')";
        $exe=mysqli_query($koneksi,$ql);


        $ambil=$koneksi->query("SELECT * FROM PEMBELIAN_PRODUK");
        $ras=$ambil->fetch_assoc();
        $JUMLAH_P=$ras['JUMLAH_P'];





        $ambil=$koneksi->query("UPDATE BUKU SET STOK =STOK-$JUMLAH
          WHERE ID_BUKU='$ID_BUKU'");
        
        
      }
      
	
		

		

		$top=$koneksi->query("INSERT INTO `ORDER`(`ID_ORDER`, `TANGGAL`, `ID_DISTRIBUTOR`,`ID_PASOK`) VALUES (NULL,'$TANGGAL_PEMBELIAN','$ID_DISTRIBUTOR','$ID_PEMBELIAN_BARUSAN')");

				if ($top) {
					echo "bener";
				}
				else
				{
					echo "salah";
				}
	


      unset($_SESSION['keranjang']);

      // TAMPILAN DI AHLIKAN KE NOTA
      echo "<script>alert('pembelian sukses');</script>";
      echo "<script>location='nota.php?id=$ID_PEMBELIAN_BARUSAN';</script>";

    }
    
     ?>
    
    
  </div>
</section>


 </body>
 </html>