<?php 
	require('../koneksi.php');

    if (!isset($_SESSION['KASIR'])) {
    echo "<script>alert('anda harus login');</script>";
    echo "<script>location='login.php';</script>";
    header('location:login.php');
    exit();
  }

	$sql="SELECT * FROM DISTRIBUTOR";
	$exe=mysqli_query($koneksi,$sql);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<h2>INFO PELANGGAN</h2>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>Id Distributor</th>
			<th>Nama Distributor</th>
			<th>Alamat</th>
			<th>Telepon</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
			<tr>
			<?php while($res=mysqli_fetch_array($exe)) : ?>
				<td><?= $res['ID_DISTRIBUTOR']?></td>
				<td><?= $res['NAMA_DISTRIBUTOR']?></td>
				<td><?= $res['ALAMAT']?></td>
				<td><?= $res['TELEPON']?></td>
				<td><a class="btn btn-danger" href="index.php?halaman=hapus_p&id=<?=$res['ID_DISTRIBUTOR'] ?>">HAPUS</a>

			</tr>
		<?php endwhile ;?>
		</tbody>
	</table>

</body>
</html>