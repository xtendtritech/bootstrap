<?php 
  require ('../koneksi.php');
 ?>
 <h2>Detail Pembelian</h2>
 <?php 
$ambil=$koneksi->query("
  SELECT * FROM PEMBELIAN_PRODUK
  JOIN PASOK ON PEMBELIAN_PRODUK.ID_PASOK=PASOK.ID_PASOK
  JOIN DISTRIBUTOR ON PASOK.ID_DISTRIBUTOR=DISTRIBUTOR.ID_DISTRIBUTOR
  JOIN BUKU ON PEMBELIAN_PRODUK.ID_BUKU = BUKU.ID_BUKU
  JOIN ONGKIR ON PASOK.ID_ONGKIR=ONGKIR.ID_ONGKIR
  WHERE PASOK.ID_PASOK='$_GET[id]'");
$detail=$ambil->fetch_assoc();
 ?>
 <br>
<table class="table table-bordered">
    <tr>
      <th>Nama Pembeli</th>
      <td><?php echo $detail['NAMA_DISTRIBUTOR']; ?></td>
    </tr>
    <tr>
      <th>Alamat</th>
      <td><?php echo $detail['ALAMAT']; ?></td>
    </tr>
    <tr>
      <th>Kode Pos</th>
      <td><?php echo $detail['KODE']; ?></td>
    </tr>
    <tr>
      <th>Telp/Hp</th>
      <td><?php echo $detail['TELEPON']; ?></td>
    </tr>
    <tr>
      <th>Email</th>
      <td><?php echo $detail['EMAIL']; ?></td>
    </tr>
    <tr>
      <th>Tanggal Pembelian</th>
      <td><?php echo $detail['TANGGAL']; ?></td>
    </tr>
    <tr>
      <th>Id Buku</th>
      <td><?php echo $detail['ID_BUKU']; ?></td>
    </tr>
    <tr>
      <th>Nama Buku</th>
      <td><?php echo $detail['JUDUL']; ?></td>
    </tr>
    <tr>
      <th>Harga Buku</th>
      <td><?php echo number_format($detail['HARGA_JUAL']); ?></td>
    </tr>
    <tr>
      <th>Jumlah Dibeli</th>
      <td><?php echo $detail['JUMLAH_P']; ?></td>
    </tr>
    <tr>
      <th rowspan="2">Ongkir</th>
      <td><?php echo $detail['NAMA_KOTA']; ?></td>
    </tr>
    <tr>
      <td>Rp. <?php echo number_format($detail['TARIF']); ?></td>
    </tr>
    <tr>
      <th>Total</th>
      <td>Rp. <?php echo number_format($detail['JUMLAH']); ?></td>
    </tr>
</table>
