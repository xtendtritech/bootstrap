<!DOCTYPE html>
<html>
<head>
<?php 
	require('../koneksi.php');
	$sql="SELECT* FROM BUKU";
	$exe=mysqli_query($koneksi,$sql);
 ?>

<title></title>
</head>
<body>
	<h2>Produk</h2>
	<table class="table table-bordered">
 			<thead>
 				<tr>
 					<th class="">Id</th>
 					<th>Judul Buku</th>
 					<!-- <th>No Isbn</th> -->
 					<th>Penulis</th>
 					<th>Penerbit</th>
 					<th>Tahun</th>
 					<th>Stock</th>
 					<!-- <th>HARGA_POKOK</th> -->
 					<th>Harga Jual</th>
 					<!-- <th>PPN</th> -->
 					<th>Diskon</th>
 					<th>Foto Produk</th>
 					<th>Aksi</th>

 				</tr>
 			</thead>
 			<tbody>
 				<tr>
 				<?php while ($res=mysqli_fetch_array($exe)): ?> 
 					
 					<td><?= $res['ID_BUKU']; ?></td>
 					<td><?= $res ['JUDUL']?></td>
 					<!-- <td><?= $res ['NOISBN']?></td> -->
 					<td><?= $res ['PENULIS']?></td>
 					<td><?= $res ['PENERBIT']?></td>
 					<td><?= $res ['TAHUN']?></td>
 					<td><?= $res ['STOK']?></td>
 					<!-- <td><?= $res ['HARGA_POKOK']?></td> -->
 					<td>Rp. <?= number_format($res ['HARGA_JUAL'])?></td>
 					<!-- <td><?= $res ['PPN']?></td> -->
 					<td><?= $res ['DISKON']?></td>
 					<td>
 						<img src="foto_produk/<?= $res ['FOTO'] ?>" width="100">
 					</td>

 					<td><a href="index.php?halaman=hapusproduk&id=<?= $res['ID_BUKU'];?>" class ="btn btn-danger">Hapus</a>
 					<a class ="btn btn-warning" href="index.php?halaman=ubahproduk&id=<?= $res['ID_BUKU'];?>">ubah</a>
 					</td>
 				
 				</tr>
 			<?php endwhile ;?>
 			</tbody>
 		</table>
 		<a class ="btn btn-primary" href="index.php?halaman=tambahproduk&id=<?=$res['ID_BUKU']?>">Tambah Data</a>

</body>
</html>

