<?php
require ('../koneksi.php');

if (!isset($_SESSION['KASIR'])) {
	echo "<script>alert('anda harus login');</script>";
	echo "<script>location='login.php';</script>";
	header('location:login.php');
	exit();
}
?>
<h2>Data Pembelian</h2>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Pembeli</th>
			<th>Jumlah</th>
			<th>Tanggal</th>
			<th>Yang Melayani</th>
			<th>Total Pembelian</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php $nomor=1; ?>
		<?php $ambil=$koneksi->query("
			SELECT * FROM PEMBELIAN_PRODUK
			JOIN PASOK ON PEMBELIAN_PRODUK.ID_PASOK=PASOK.ID_PASOK
			JOIN DISTRIBUTOR ON PASOK.ID_DISTRIBUTOR=DISTRIBUTOR.ID_DISTRIBUTOR"); ?>
			<?php while($pecah = $ambil->fetch_assoc()) { ?>
				<tr>
					<td><?php echo $pecah['ID_PEMBELIAN_PRODUK']; ?></td> 
					<td><?php echo $pecah['NAMA_DISTRIBUTOR']; ?></td>
					<td><?php echo $pecah['JUMLAH_P']; ?></td>
					<td><?php echo $pecah['TANGGAL']; ?></td>
					<td><?php echo $pecah['PELAYAN']; ?></td>
					<td>Rp. <?php echo number_format($pecah['JUMLAH']); ?></td>
					
					<td><?php echo $pecah['STATUS_PEMBELIAN']; ?></td>
					<td>
						<a href="index.php?halaman=detail&id=<?php echo $pecah['ID_PASOK']; ?>" class="btn-info btn">Detail</a>
						<?php if($pecah['STATUS_PEMBELIAN']=="Sudah Mengirim Pembayaran"): ?>
							<a href="index.php?halaman=pembayaran&id=<?php echo $pecah['ID_PASOK'] ?>" class="btn btn-success">View</a>
						<?php endif ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
