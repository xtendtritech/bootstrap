﻿<?php 
  session_start();
  require ('../koneksi.php');
 ?>  


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMIN SHOPBOOK</title>
	<!-- BOOTSTRAP stylesheet-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body>
    <div class="container">
        <div class="row text-center ">
            <div class="col-md-12">
                <br /><br />
                <h2> SHOPBOOK : LOGIN</h2>
               
                <h5>( Login yourself to get access )</h5>
                 <br />
            </div>
        </div>
         <div class="row ">
               
                  <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                        <strong>   Enter Details To Login </strong>  
                            </div>
                            <div class="panel-body">
                                <form role="form" method="POST">
                                       <br />
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control" placeholder="Username " name="USERNAME" />
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" class="form-control"  placeholder="Password" name="PASSWORD" />
                                        </div>
                                    <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" /> Remember me
                                            </label>
                                            <span class="pull-right">
                                                   <a href="#" >Forget password ? </a> 
                                            </span>
                                        </div>
                                     
                                    <button  class="btn btn-primary " name="login"> Login</button>
                                    <hr />
                                    Not register ? <a href="registeration.php" >click here </a> 
                                    </form>
                                    <?php 
                                    if (isset($_POST['login']))
                                    {
                                      $sql=$koneksi->query("SELECT* FROM KASIR WHERE USERNAME='$_POST[USERNAME]'AND PASSWORD='$_POST[PASSWORD]'");
                                      $ygcocok= $sql->num_rows;
                                      
                                      
                                        
                                      if ($ygcocok==1) {
                                        $_SESSION['KASIR']=$sql->fetch_assoc();
                                        echo "<script>alert('anda telah login');</script>"; 
                                        echo "<meta http-equiv='refresh' content='1,url=index.php'>";
                                      }
                                      
    
                                      else
                                      {
                                       echo "<div class='alert alert-danger'>LOGIN GAGAL</div>"; 
                                        echo "<meta http-equiv='refresh' content='1,url=login.php'>"; 
                                      }
                                    }


                                     ?>
                            </div>
                           
                        </div>
                    </div>
                
                
        </div>
    </div>


     <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
</body>
</html>
