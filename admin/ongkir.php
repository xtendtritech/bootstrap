<?php
    require ('../koneksi.php');

     if (!isset($_SESSION['KASIR'])) {
    echo "<script>alert('anda harus login');</script>";
    echo "<script>location='login.php';</script>";
    header('location:login.php');
    exit();
  }
  ?>
<h2>Ongkos Kirim</h2>

<?php 
	require ('../koneksi.php');

	$sql = "SELECT* FROM ONGKIR";
	$exe = mysqli_query($koneksi,$sql);
 ?>
 <table class="table table-bordered">
 	<thead>
 	<tr>
 		<th>Id Ongkir</th>
 		<th>Nama Kota</th>
 		<th>Tarif</th>
 	</tr>
 	</thead>
 	<tbody>
 		<?php while($res=mysqli_fetch_array($exe)) : ?>
 		<tr>
 			<td><?= $res['ID_ONGKIR']; ?></td>
 			<td><?= $res['NAMA_KOTA']; ?></td>
 			<td>Rp.<?= number_format($res['TARIF']); ?></td>
 		</tr>
 	<?php endwhile; ?>
 	</tbody>
 </table>