<?php 
session_start();
include 'koneksi.php';

 if (!isset($_SESSION["DISTRIBUTOR"]) OR empty($_SESSION["DISTRIBUTOR"])  ) {
    echo "<script>alert('anda harus login');</script>";
    echo "<script>location='login.php';</script>";
    header('location:login.php');
    exit();
  }


  //mendapatkan id_pembelian dari url
  $idpem = $_GET['id'];
  $ambil = $koneksi->query("SELECT * FROM PASOK WHERE ID_PASOK='$idpem'");
  $detpem = $ambil->fetch_assoc();

  //mendapatkan id_pelanggan yang beli
  $id_pelanggan_beli = $detpem["ID_DISTRIBUTOR"];
  //mendapatkan  id_pelanggan yang login
  $id_pelanggan_login = $_SESSION["DISTRIBUTOR"]["ID_DISTRIBUTOR"];

  if ($id_pelanggan_login !== $id_pelanggan_beli) 
  {
  	 echo "<script>alert('Jangan Nakal Ya');</script>";
     echo "<script>location='riwayat.php';</script>";
  }

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Pembayaran</title>
 	 <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
 </head>
 <body>

 	<?php include 'navbar.php'; ?>

 	<div class="container">
 		<h2>Konfirmasi Pembayaran</h2>
 		<p>Kirim Bukti Pembayaran Anda Disini</p>
 		<div class="alert alert-info">Total Tagihan Anda <strong>Rp. <?php echo number_format($detpem["JUMLAH"]) ?></strong></div>

 		<form method="post" enctype="multipart/form-data">
 			<div class="form-group">
 				<label>Nama Pembeli</label>
 				<input type="text" name="nama" class="form-control" readonly value="<?php echo $_SESSION['DISTRIBUTOR']['NAMA_DISTRIBUTOR']?>">
 			</div>
 			<div class="form-group">
 			    <label>Nama Bank</label>
 				<select class="form-control" name="bank">
 				<option>MUAMALAT</option>
 				<option>BNI</option>
 				<option>MANDIRI</option>
 				<option>ACEH</option>
 				</select>
 			</div>
 			<div class="form-group">
 				<label>Jumlah (Rp)</label>
 				<input type="number" name="jumlah" class="form-control" readonly value="<?php echo $detpem['JUMLAH']; ?>">
 			</div>
 			<div class="form-group">
 				<label>Foto Bukti</label>
 				<input type="file" name="bukti" class="form-control" required>
 				<p class="text-danger">Foto Bukti Harus JPG Maksimal 2MB</p>
 			</div>
 			<button class="btn btn-primary" name="kirim">Kirim</button>
 		</form>
 	</div>

<?php
	//jika ada tombol kirim

	if (isset($_POST["kirim"])) 
	{
		//upload dulu foto buktinya
		$namabukti = $_FILES["bukti"]["name"];
		$lokasibukti = $_FILES["bukti"]["tmp_name"];
		$namafiks = date("YmdHis").$namabukti;
		move_uploaded_file($lokasibukti, "bukti_pembayaran/$namafiks");

		$nama = $_POST["nama"];
		$bank = $_POST["bank"];
		$jumlah = $_POST["jumlah"];
		$tanggal = date("Y-m-d");


		$koneksi->query("INSERT INTO PENJUALAN(ID_PASOK,NAMA,BANK,TOTAL,TANGGAL,BUKTI)
			VALUES('$idpem','$nama','$bank','$jumlah','$tanggal','$namafiks')");

		//update data pembelian dari pending menjadi sudah lunas
		$koneksi->query("UPDATE PASOK SET STATUS_PEMBELIAN ='Sudah Mengirim Pembayaran'
			WHERE ID_PASOK = '$idpem'");

		echo "<script>alert('Terima Kasih Telah Mengrim Pembayaran');</script>";
     echo "<script>location='riwayat.php';</script>";
	}
	
 ?>
 
 </body>
 </html>
