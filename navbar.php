<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="web.php">SHOPBOOK</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="web.php">Home</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Belanja<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="keranjang.php">Keranjang</a></li>
          <li><a href="checkout.php">Checkout</a></li>
        </ul>
      </li>
      <li><a href="riwayat.php">Riwayat Belanja</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="daftar.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <?php if (isset($_SESSION["DISTRIBUTOR"])): ?>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        <?php else: ?> 
        <li class="nav-item"><a class="glyphicon glyphicon-log-out" href="login.php">Login</a></li>
            <?php endif ?>
    </ul>
  </div>
</nav>
  
</body>
</html>
