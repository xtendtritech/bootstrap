<?php 
	session_start();
	require('koneksi.php');

	if (!isset($_SESSION["DISTRI"]) OR empty($_SESSION["DISTRI"])  ) {
    echo "<script>alert('anda harus login');</script>";
    echo "<script>location='login.php';</script>";
    header('location:login.php');
    exit();}
	
	
 ?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>
   <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="web.php">SHOPBOOK</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <?php if (isset($_SESSION["DISTRI"])): ?>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        <?php else: ?> 
        <li class="nav-item"><a class="glyphicon glyphicon-log-out" href="login.php">Login</a></li>
            <?php endif ?>
    </ul>
  </div>
</nav>
   
   
<div class="container">
	<h2>Produk</h2>
	<table class="table table-bordered" width="100%" border="1">
 			<thead>
 				<tr>
 					<th class="">Id</th>
 					<th>Judul Buku</th>
 					<th>Penulis</th>
 					<th>Penerbit</th>
 					<th>Tahun</th>
 					<th>Stock</th>
 					<!-- <th>HARGA_POKOK</th> -->
 					<th>Harga Jual</th>
 					<!-- <th>PPN</th> -->
 					
 					<th>foto</th>
 					<th>Aksi</th>

 				</tr>
 			</thead>
 			<tbody>
 				<tr>
 				<?php 
 					// $res=$_GET['id'];
 				 $pelanggan =$_SESSION["DISTRI"]["ID_DISTRI"];
 				 
 				 
 				 $sql=$koneksi->query("SELECT * FROM BUKU JOIN DISTRI ON BUKU.ID_DISTRI=DISTRI.ID_DISTRI WHERE BUKU.ID_DISTRI='$pelanggan'");
 				 

 				 while ($res=$sql->fetch_assoc()) { ?>

 				 <td><?php echo $res['ID_BUKU']; ?></td>
 				
 					<td><?= $res ['JUDUL']?></td>
 					<td><?= $res ['PENULIS']?></td>
 					<td><?= $res ['PENERBIT']?></td>
 					<td><?= $res ['TAHUN']?></td>
 					<td><?= $res ['STOK']?></td>
 					<!-- <td><?= $res ['HARGA_POKOK']?></td> -->
 					<td><?= $res ['HARGA_JUAL']?></td>
 					<!-- <td><?= $res ['PPN']?></td> -->
 					
 					<td>
 						<img src="admin/foto_produk/<?= $res ['FOTO']?>" width="100" >
 					</td>
 					<td><a href="admin/hapusproduk.php?id=<?= $res['ID_BUKU'];?>" class ="btn btn-danger">Hapus</a>
 					<a class ="btn btn-warning" href="admin/ubahproduk.php?id=<?php echo $res['ID_BUKU'];?>">ubah</a>
 					</td>

 				 	</tr>
 				 <?php }?>	

 				
 			</tbody>
 		</table>
 		<a class ="btn btn-primary" href="admin/tambahproduk.php?id=<?=$res['ID_BUKU']?>">Tambah Data</a>

 </div>
 		

</body>
</html>

