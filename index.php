<?php 

	session_start();
	require ('koneksi.php');
	
  
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>SHOP BOOK</title>
	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>

	<!-- navbar -->
		<?php include 'navbar.php'; ?>

	<!-- konten -->

<section class="konten">
	<div class="container">
		<h1>Produk Terbaru</h1>
		<div class="row">
			<?php 
				$sql = "SELECT * FROM BUKU";
				$exe = mysqli_query($koneksi,$sql);
			 ?>
			 <?php while($res=mysqli_fetch_array($exe)): ?>
			 	<!-- <pre><?php print_r($res) ?></pre>-->			
        <div class="col-sm-3">
  				<div class="thumbnail">
  					<img style="height: 200px" src="admin/foto_produk/<?= $res['FOTO'] ?>">
  					<div class="caption">
  						<h3><?= $res['JUDUL'] ?></h3>
  						<h5>Rp.<?= number_format( $res['HARGA_JUAL'])?></h5>
  						<a href="beli.php?id=<?=$res['ID_BUKU'] ?>" class="btn btn-primary">Beli</a>
  						<a class="btn btn-default" href="detail.php?id=<?=$res['ID_BUKU'] ?>">Detail</a>
  					</div>
  				</div>
			 </div>
		<?php endwhile; ?>
  </div>
</div>	
</section>

</body>
</html>