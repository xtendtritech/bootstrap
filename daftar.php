<?php require ('koneksi.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Daftar</title>
	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>
	<?php include 'navbar.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Registrasi Pelanggan</h3>
				</div>
				<div class="panel-body">
					<form method="post" class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-md-3">Nama</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="nama" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Password</label>
							<div class="col-md-7">
								<input type="password" class="form-control" name="pass" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Alamat</label>
							<div class="col-md-7">
								<textarea class="form-control" name="alamat" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Kode Pos</label>
							<div class="col-md-7">
								<textarea class="form-control" name="kode" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Telepon</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="telepon" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-7 col-md-offset-3">
								<button class="btn btn-primary" name="daftar">Daftar</button>
							</div>
						</div>
						<hr />
						already have an account ?<a href="login.php" >click here </a> 
					</form>
					<?php 
					if (isset($_POST['daftar'])) 
					{
						$nama = $_POST['nama'];
						$pass = $_POST['pass'];
						$alamat = $_POST['alamat'];
						$kode = $_POST['kode'];
						$telepon = $_POST['telepon'];

						$sql = "INSERT INTO DISTRIBUTOR (ID_DISTRIBUTOR,NAMA_DISTRIBUTOR,PASSWORD,ALAMAT,KODE,TELEPON)VALUES(NULL,'$nama','$pass','$alamat','$kode','$telepon')";
						$exe = mysqli_query($koneksi,$sql);

						if ($exe) 
						{
							echo "<script>alert('Anda Berhasil Menjadi Member');</script>";
      						echo "<script>location='web.php';</script>";
						}
						else{
							 echo "<script>alert('Periksa Kembali Data Anda');</script>";
     						 echo "<script>location='daftar.php';</script>";
						}
					}
					 ?>
				</div>
			</div>
		</div>		
	</div>
</div>
</body>
</html>