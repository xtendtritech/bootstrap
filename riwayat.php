<?php 
require('koneksi.php');
session_start();


if (!isset($_SESSION["DISTRIBUTOR"]) OR empty($_SESSION["DISTRIBUTOR"])  ) {
	echo "<script>alert('anda harus login');</script>";
	echo "<script>location='login.php';</script>";
	header('location:login.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Riwayat</title>
	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>
	<?php include 'navbar.php'; ?>
	<!-- <pre><?php print_r($_SESSION) ?></pre> -->
	<section class="col-lg-6">
  <div class="container">
  <div class="table-responsive">
			<h3>Riwayat Belanja <strong><?php  echo $_SESSION['DISTRIBUTOR']['NAMA_DISTRIBUTOR']; ?></strong></h3>
			<table class="table table-bordered">
				<thead>
					<th>No</th>
					<th>Nama Buku</th>
					<th>Harga Buku</th>
					<th>Tanggal</th>
					<th>Status</th>
					<th>Total</th>
					

				</thead>
				<tbody>

					<?php 
					$nomor=1;
					$pelanggan =  $_SESSION['DISTRIBUTOR']['ID_DISTRIBUTOR'];

					$sql=$koneksi->query("
						SELECT * FROM PEMBELIAN_PRODUK
						JOIN PASOK ON PEMBELIAN_PRODUK.ID_PASOK=PASOK.ID_PASOK
						JOIN BUKU ON PEMBELIAN_PRODUK.ID_BUKU=BUKU.ID_BUKU
						JOIN ORDER ON ORDER.ID_PASOK = ORDER.ID_ORDER
						WHERE PEMBELIAN_PRODUK.ID_PASOK='$_GET[id]'");
						while ($res= $sql->fetch_assoc()){?>
							<td><?php echo $nomor;	 ?></td>
							<td><?php echo $res['JUDUL']; ?></td>
							<td>Rp. <?php echo number_format($res['HARGA_JUAL']); ?></td>
							<td><?php echo $res['TANGGAL'] ?></td>
							<td>
								<?php echo $res['STATUS_PEMBELIAN'] ?><br>
								<?php if(!empty($res['RESI_PENGIRIMAN'] AND $res['PELAYAN'])):?>
									Resi : <?php echo $res['RESI_PENGIRIMAN']; ?><br>
									Kasir : <?php echo $res['PELAYAN']; ?>
								<?php endif ?>	
							</td>
							<td>Rp. <?php echo number_format($res['JUMLAH']); ?></td>
							
								<?php if($res['STATUS_PEMBELIAN']=="PENDING"): ?>
								
								<?php endif ?>
								
							</tbody>
							<?php $nomor++ ?>
						<?php } ?>
					</table>
					<a class="btn btn-info" href="order.php?id=<?php echo $res['ID_ORDER']?>">Back</a>
				</div>
				</div>
			</section>

		</body>
		</html>