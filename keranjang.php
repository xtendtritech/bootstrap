<?php 
session_start();
require('koneksi.php');


if (empty($_SESSION['keranjang']))
{
  echo "<script>alert('Keranjang Anda Kosong Silahkan Belanja');</script>";
  echo "<script>location='web.php';</script>";
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>Keranjang</title>
  <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>
 <?php include 'navbar.php'; ?>

 <section class="col-lg-6">
  <div class="container">
  <div class="table-responsive">
    <h1>keranjang Belanja</h1>
    <hr>
    <table border="1" width="100%" class="table table-border">
      <thead>
        <tr>
          <th>No</th>
          <th>Produk</th>
          <th>Harga</th>
          <th>Jumlah</th>
          <th>Subharga</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $nomor=1;?>
        <?php foreach($_SESSION['keranjang'] AS $ID_BUKU => $JUMLAH): ?>
          <?php $ambil= $koneksi->query("SELECT * FROM BUKU WHERE ID_BUKU='$ID_BUKU'"); 
          $pecah= $ambil->fetch_assoc();
          $SUBHARGA= $pecah['HARGA_JUAL']*$JUMLAH;    
          ?> 
          <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $pecah['JUDUL'] ?></td>
            <td>Rp.<?php echo number_format($pecah['HARGA_JUAL']) ?></td>
            <td><?php echo $JUMLAH; ?></td>
            <td>Rp. <?php echo number_format($SUBHARGA); ?></td>
            <td>
              <a class="btn btn-danger" href="hapusbarang.php?id=<?php echo$ID_BUKU?>">Hapus</a>
            </td>
          </tr>
          <?php $nomor++; ?>
        <?php endforeach ?>
      </tbody>        
    </table>
    <a href="web.php" class="btn btn-default">Lanjutkan Belanja</a>
    <a href="checkout.php" class="btn btn-primary">CheckOut</a>

  </div>

</section>

</body>
</html>